#pragma once

#include <vector>

#define HEADER_SIZE 150 //in bytes

struct PulseData
{
	uint16_t length;
	uint16_t start;
	std::vector<uint16_t> samples;
};

typedef std::vector<PulseData> PulseVec;