# Usage:
- compile with `make`
- run `./DummyEventCreator` to generate `dummy_event.bin` binary file with dummy, zero-suppresed data
- run `./EventReader` to read the event and plot the waveforms into the `wvfms.pdf` file

Waveforms consist of a single gaussian at random locations and of random amplitudes. Generated values are shifted by 100 ADC counts to emphasize the generated region.

Header is empty (filled with zeros) apart from the data length and event ID.

There is **no zero suppression** implemented, pieces of waveform are simply generated.
