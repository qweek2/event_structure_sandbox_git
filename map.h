#include <vector>
#include <map>

typedef uint16_t BoardId;
typedef uint16_t SocketId;
typedef uint16_t ModuleId;
typedef uint16_t SectionId;
typedef uint16_t ChannelId;

// How are the modules connected to the boards (via the sockets)
extern std::map<BoardId, std::map<SocketId, ModuleId>> ModuleMapping = {
    {1280, {{1, 21}, {2, 34}, {3, 33}}},
    {1281, {{1, 36}, {2, 35}, {3, 22}}},
    {1282, {{1, 20}, {2, 32}, {3, 31}}},
    {1283, {{1, 38}, {2, 37}, {3, 23}}},
    {1284, {{1, 30}, {2, 18}, {3, 19}}},
    {1285, {{1, 16}, {2, 14}, {3, 13}}},
    {1286, {{1, 24}, {2, 25}, {3, 39}}},
    {1287, {{1, 44}, {2, 17}, {3, 29}}},
    {1288, {{1, 28}, {2, 27}, {3, 15}}},
    {1289, {{1, 40}, {2, 26}, {3, 41}}},
    {1290, {{1, 43}, {2, 42}}}};

// How are the channels mapped in the sockets
extern std::map<ChannelId, std::pair<SocketId, SectionId>> ChannelMapping = {
    {0, {1, 1}},
    {1, {1, 2}},
    {2, {1, 3}},
    {3, {1, 4}},
    {4, {1, 5}},
    {5, {1, 6}},
    {6, {1, 7}},
    {7, {1, 8}},
    //{  8, {0, 0} },  // Time Ref 1
    {9, {1, 9}},
    {10, {1, 10}},
    //{ 11, {0, 0} },  // NC
    {12, {2, 1}},
    {13, {2, 2}},
    {14, {2, 3}},
    {15, {2, 4}},
    {16, {2, 5}},
    //{ 17, {0, 0} },  // Time Ref 2
    {18, {2, 6}},
    {19, {2, 7}},
    {20, {2, 8}},
    {21, {2, 9}},
    {22, {2, 10}},
    //{ 23, {0, 0} },  // NC
    {24, {3, 1}},
    {25, {3, 2}},
    //{ 26, {0, 0} },  // Time Ref 3
    {27, {3, 3}},
    {28, {3, 4}},
    {29, {3, 5}},
    {30, {3, 6}},
    {31, {3, 7}},
    {32, {3, 8}},
    {33, {3, 9}},
    {34, {3, 10}},
    //{ 35, {0, 0} },
};