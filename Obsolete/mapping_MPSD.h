#include <utility>

std::pair<int, int> getDRSIndex (int mod, int sect) {

int iBoard = -1;
int iCh = -1;

if (mod == 21) { iBoard = 1280; iCh = sect; }
if (mod == 34) { iBoard = 1280; iCh = sect + 11; }
if (mod == 33) { iBoard = 1280; iCh = sect + 22; }

return std::make_pair(iBoard, iCh);

}

