#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <numeric>
#include <stdio.h>
#include "utils.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TString.h"
#include "TH1D.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"

typedef uint16_t BoardId ;
typedef uint16_t SocketId ;
typedef uint16_t ModuleId ;
typedef uint16_t SectionId ;
typedef uint16_t ChannelId ;

// How are the modules connected to the boards (via the sockets)
std::map<BoardId, std::map<SocketId, ModuleId> > ModuleMapping = {
  { 1280, {{1,21}, {2,34}, {3,33}} },
  { 1281, {{1,36}, {2,35}, {3,22}} },
  { 1282, {{1,20}, {2,32}, {3,31}} },
  { 1283, {{1,38}, {2,37}, {3,23}} },
  { 1284, {{1,30}, {2,18}, {3,19}} },
  { 1285, {{1,16}, {2,14}, {3,13}} },
  { 1286, {{1,24}, {2,25}, {3,39}} },
  { 1287, {{1,44}, {2,17}, {3,29}} },
  { 1288, {{1,28}, {2,27}, {3,15}} },
  { 1289, {{1,40}, {2,26}, {3,41}} },
  { 1290, {{1,43}, {2,42}} }
};

// How are the channels mapped in the sockets
std::map<ChannelId, std::pair<SocketId, SectionId> > ChannelMapping = {
  {  0, {1, 1} },
  {  1, {1, 2} },
  {  2, {1, 3} },
  {  3, {1, 4} },
  {  4, {1, 5} },
  {  5, {1, 6} },
  {  6, {1, 7} },
  {  7, {1, 8} },
//{  8, {0, 0} },  // Time Ref 1
  {  9, {1, 9} },
  { 10, {1,10} },
//{ 11, {0, 0} },  // NC
  { 12, {2, 1} },
  { 13, {2, 2} },
  { 14, {2, 3} },
  { 15, {2, 4} },
  { 16, {2, 5} },
//{ 17, {0, 0} },  // Time Ref 2
  { 18, {2, 6} },
  { 19, {2, 7} },
  { 20, {2, 8} },
  { 21, {2, 9} },
  { 22, {3,10} },
//{ 23, {0, 0} },  // NC
  { 24, {3, 1} },
  { 25, {3, 2} },
//{ 26, {0, 0} },  // Time Ref 3
  { 27, {3, 3} },
  { 28, {3, 4} },
  { 29, {3, 5} },
  { 30, {3, 6} },
  { 31, {3, 7} },
  { 32, {3, 8} },
  { 33, {3, 9} },
  { 34, {3,10} },
//{ 35, {0, 0} },
};

/* Example: 
 * with board_id and channel_id: 
 *
   if(ChannelMapping.find(channel_id) == ChannelMapping.end()) break; // Channel NC
   auto [socket_id, section_id] = ChannelMapping[channel_id];
   if(ModuleMapping.find(board_id) == ModuleMapping.end()) break; // Unknown board
   auto module_id = ModuleMapping[board_id][socket_id];
*/

std::map<uint8_t,PulseVec> ParseData(std::vector<uint16_t> data)
{
	std::map<uint8_t,PulseVec> result;
	unsigned int word=0;
	while(word<data.size())
	{
		//std::cout<<"word: "<<word<<std::endl;
		int chan=data[word+2]+9*data[word+3];
		uint32_t words_to_read = data[word];
		words_to_read |= uint32_t(data[word+1])<<12;

		//std::cout<<"words to read: "<<words_to_read<<std::endl;

		PulseVec v;
		PulseData pul;
		int samples_to_read=0;
		std::vector<uint16_t> samples;
		word+=4;
		for(unsigned int i=0;i<words_to_read*4;)
		{
			if(samples_to_read==0)
			{
				pul.length=data[word+i];
				pul.start=data[word+i+1];
				i+=2;
				samples_to_read=pul.length;
				//std::cout<<"samples to read: "<<samples_to_read<<std::endl;
				continue;
			}
			samples.push_back(data[i+word]);
			i++;
			samples_to_read--;
			if(samples_to_read==0)
			{
				pul.samples=samples;
				v.push_back(pul);
				samples.clear();
			}

		}
		word+=words_to_read*4;
		result[chan]=v;
	}
	return result;

}
float GetPedestal(std::vector<uint16_t> v)
{
  float avg = 0;
  for(unsigned int i=10;i<50;i++)
  {
    avg += v[i];
  }
  return avg/40;
}

float GetSignal(std::vector<uint16_t> v)
{
  float max = -10000;
  for(unsigned int i=50;i<v.size()-10;i++)
  {
    if(v[i] > max) max = v[i];
  }
  return max;
}

std::vector<uint16_t> BuildChannel(PulseVec vec)
{
	std::vector<uint16_t> result(1024,0);
	for(auto pul : vec)
	{
		for(unsigned int i=0;i<pul.length;i++)
		{
			result[pul.start+i]=pul.samples[i];
		}
	}
	return result;
}

// First pass over input file (first event only) to get the list of board ids recorded
//
std::vector<uint16_t> ReadBoardIds(std::ifstream& input) {
    std::cout << "=== First pass: get board IDs ===" << std::endl;
    std::vector<uint16_t> board_ids;

    input.seekg(0, std::ios::end);
    auto pos = input.tellg();
    std::vector<uint8_t>  data(pos);
    input.seekg(0, std::ios::beg);
    input.read(reinterpret_cast<char*>(&data[0]), pos);

    int offsetDtata = 0;
    int iEvent = 0;

    std::map<int, int> boardEventId;
    int firstEvID = -1;

    while (offsetDtata < pos) {

      auto n_bytes = *(reinterpret_cast<uint32_t*>(&data[offsetDtata]));
      auto evID = *(reinterpret_cast<uint64_t*>(&data[offsetDtata + 19*6]));
      auto iBoard = *(reinterpret_cast<uint16_t*>(&data[offsetDtata + 1*6 + 1])) & 0xfff;

      std::cout<<"Event with trigNumber =  "<< evID << ", iBoard = 0x" << std::hex << iBoard << std::dec << " contains "<<n_bytes<<" data bytes (excluding header)"<<std::endl;

      if(firstEvID < 0) firstEvID = evID;
      
//        std::cout << "ok " << firstEvID << " " << evID << std::endl;
      if(firstEvID != evID) { 
//        std::cout << "brk " << firstEvID << " " << evID << std::endl;
        //second event already, quit as we got all the board ids  
        break;
      }

      board_ids.push_back(iBoard);     

      offsetDtata += HEADER_SIZE + n_bytes;
    }

    input.seekg(0, std::ios::beg);
    std::cout << "=== First pass: done (" << board_ids.size() << " boards)" << std::endl;

    std::sort(board_ids.begin(), board_ids.end());
    return board_ids;
}

TString TreeStructureForBoards(std::vector<uint16_t> board_ids) {
    TString result;
    int nChannels = 36;
    for(int i = 0; i < board_ids.size(); i++) {
        for(int sec = 0; sec < nChannels; sec++) {
            result += TString::Format("board_%d_ch_%d/f:", board_ids[i], sec);
        }
    }
    return result;
}


int FindBoardOrderById(std::vector<uint16_t> board_ids, uint16_t id) {
    for(int i = 0; i < board_ids.size(); i++) {
        if(board_ids[i] == id) { return i; }
    }
    return -1;
}

int main(int argc, char **argv){
    if (argc < 2){
        std::cout << "Usage: " << argv[0] << " <File Name> \n";
        return -1;
    }

    std::string fname = argv[1];

    std::cout << "File name = " << fname << std::endl;

    auto hists = std::map<std::string, TH1D*>();
    std::ifstream input(fname, std::ios::binary | std::ios::ate);
    auto pos = input.tellg();
    std::cout << "input.tellg() = " << pos << std::endl;
    auto board_ids = ReadBoardIds(input);
    TFile f("hists.root", "RECREATE");
    TTree tree("Modules", "Modules");

    auto tree_structure = TreeStructureForBoards(board_ids);    
    std::cout << tree_structure;
    float *tree_buf = new float[board_ids.size()*36];
    tree.Branch("Test", tree_buf, tree_structure);
    std::cout << "===Second pass: Read Events ===" << std::endl;
    std::vector<uint8_t>  data(pos);
    input.seekg(0, std::ios::beg);
    input.read(reinterpret_cast<char*>(&data[0]), pos);

    int offsetDtata = 0;
    int iEvent = 0;
    int lastEvID = -1;

    std::map<int, int> boardEventId;

    while (offsetDtata < pos) {

      auto n_bytes = *(reinterpret_cast<uint32_t*>(&data[offsetDtata]));
      auto evID = *(reinterpret_cast<uint64_t*>(&data[offsetDtata + 19*6]));
      auto iBoard = *(reinterpret_cast<uint16_t*>(&data[offsetDtata + 1*6 + 1])) & 0xfff;

      auto boardOrder = FindBoardOrderById(board_ids, iBoard);
      int nChannels = 36;

      if(!(evID%10)) std::cout<<"[" << (offsetDtata*100L/pos) << "%] Event with trigNumber =  "<< evID << ", iBoard = 0x" << std::hex << iBoard << std::dec << " contains "<<n_bytes<<" data bytes (excluding header)"<<std::endl;

      auto search = boardEventId.find(iBoard);
      if(search != boardEventId.end()) {
        auto iEvent = search->second + 1;
        boardEventId[iBoard] = iEvent;
      } else {
        boardEventId[iBoard] = 0;
      }
      if(lastEvID < 0) {
          lastEvID = evID;
      }
      if(lastEvID != evID) {
          // new event; need to write down the last one
          tree.Fill(); 
          lastEvID = evID;
          //std::cout << "Tree.Fill()" << lastEvID << " " << evID << std::endl;
      }
      //std::cout << "Will process board " << iBoard << ", event " << boardEventId[iBoard] << std::endl;

      offsetDtata += HEADER_SIZE + n_bytes;


    gStyle->SetTickLength(0.001,"x");

    auto data_bytes = *(reinterpret_cast<uint32_t*>(&data[offsetDtata]));

    std::vector<uint16_t> data_16;

    for(unsigned int i = offsetDtata; i < offsetDtata + data_bytes; i+=3)
    {
    	uint16_t b0, b1, b2;
    	uint16_t samp0=0, samp1=0;
    	b0=data[HEADER_SIZE+i];
    	b1=data[HEADER_SIZE+i+1];
    	b2=data[HEADER_SIZE+i+2];


    	samp0=b0 |  ((b1) & 0x0F) <<8;
    	samp1=( (b1>>4) & 0xFF ) | b2<<4;
    	data_16.push_back(samp0&0xfff);
    	data_16.push_back(samp1&0xfff);


    }


    auto m = ParseData(data_16);
    std::vector<uint16_t> tslice(1024);
    std::iota(tslice.begin(),tslice.end(),1);

    for(auto& [ch,vec]:m )
    {
        // Find PSD module and section connected to given board and channel
        if(ChannelMapping.find(ch) == ChannelMapping.end()) {
            //std::cout << "Channel mapping: ch #" << int(ch) << "NC, skipping" << std::endl;
            continue; // Channel NC
        }
        auto [socket_id, section_id] = ChannelMapping[ch];
        if(ModuleMapping.find(iBoard) == ModuleMapping.end()) {
            std::cout << "Module mapping: board #" << int(iBoard) << " not recognized" << std::endl;
            continue; // Unknown board
        }
        auto module_id = ModuleMapping[iBoard][socket_id];

        //std::cout << "Board " << iBoard << " channel " << int(ch) << " maps to PSD module " << module_id << " section " << section_id << std::endl;

    	auto v = BuildChannel(vec);
        auto value = GetSignal(v) - GetPedestal(v);
        char hist_name[100];

        // Find or create per-section histogram
        //snprintf(hist_name, 101, "h_%d_%d", iBoard, ch+1);
        sprintf(hist_name, "hEdepSect%d", module_id*100 + section_id);
        auto search = hists.find(std::string(hist_name));
        if(search == hists.end()) {
          std::cout << "Creating hist " << hist_name << " (" << hists.size() << ")" <<std::endl;
          hists[hist_name] = new TH1D(hist_name, hist_name, 4096, 1, 4096);
        }

        //std::cout << "Filling hist " << h_name << " with value " << value << std::endl;
        hists[hist_name]->Fill(value); 
        
        // Find or create per-module histogram
        // TODO sum up the modules
        sprintf(hist_name, "hEdepMod%d", module_id);
        search = hists.find(std::string(hist_name));
        if(search == hists.end()) {
          std::cout << "Creating hist " << hist_name << " (" << hists.size() << ")" <<std::endl;
          hists[hist_name] = new TH1D(hist_name, hist_name, 4096, 1, 4096);
        }

        //std::cout << "Filling hist " << hist_name << " with value " << value << std::endl;
        hists[hist_name]->Fill(value); 
        
        // Fill the tree
        tree_buf[boardOrder*36+ch] = value;
    }
    }
    tree.Write();
    f.Write();
}
