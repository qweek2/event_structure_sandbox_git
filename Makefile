CXX = g++
CXXFLAGS = -g -fPIC -O3 -std=c++17

LD = g++
LDFLAGS = -g

CXXFLAGS += $(shell root-config --cflags)
LIBS = $(shell root-config --libs)


SOURCES=$(wildcard *.cpp)
OBJECTS=$(SOURCES:.cpp=.o)
HEADERS=$(SOURCES:.cpp=.h)
EXECUTABLE=DummyEventCreator EventReader PSDEventReader EventReader_Sergey PSDEventReader_Sergey PSDEventReader_hists



all: $(SOURCES) $(EXECUTABLE)

EventReader: EventReader.o
	$(CXX) $(CXXFLAGS) $^ $(LIBS) -o $@ 

EventReader_Sergey: EventReader_Sergey.o
	$(CXX) $(CXXFLAGS) $^ $(LIBS) -o $@ 

PSDEventReader: PSDEventReader.o
	$(CXX) $(CXXFLAGS) $^ $(LIBS) -o $@ 

PSDEventReader_Sergey: PSDEventReader_Sergey.o
	$(CXX) $(CXXFLAGS) $^ $(LIBS) -o $@ 

PSDEventReader_hists: PSDEventReader_hists.o
	$(CXX) $(CXXFLAGS) $^ $(LIBS) -o $@ 

.cpp.o: $(SOURCES) $(HEADERS)
	$(CXX) -c $(CXXFLAGS) $< -o $@ $(LIBS) -I$(PWD)

clean:
	rm -f $(EXECUTABLE) *.o
