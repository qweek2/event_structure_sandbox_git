#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <numeric>
#include <stdio.h>
#include "utils.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TString.h"
#include "TH1D.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"

std::map<uint8_t,PulseVec> ParseData(std::vector<uint16_t> data)
{
	std::map<uint8_t,PulseVec> result;
	unsigned int word=0;
	while(word<data.size())
	{
		//std::cout<<"word: "<<word<<std::endl;
		int chan=data[word+2]+9*data[word+3];
		uint32_t words_to_read = data[word];
		words_to_read |= uint32_t(data[word+1])<<12;

		//std::cout<<"words to read: "<<words_to_read<<std::endl;

		PulseVec v;
		PulseData pul;
		int samples_to_read=0;
		std::vector<uint16_t> samples;
		word+=4;
		for(unsigned int i=0;i<words_to_read*4;)
		{
			if(samples_to_read==0)
			{
				pul.length=data[word+i];
				pul.start=data[word+i+1];
				i+=2;
				samples_to_read=pul.length;
				//std::cout<<"samples to read: "<<samples_to_read<<std::endl;
				continue;
			}
			samples.push_back(data[i+word]);
			i++;
			samples_to_read--;
			if(samples_to_read==0)
			{
				pul.samples=samples;
				v.push_back(pul);
				samples.clear();
			}

		}
		word+=words_to_read*4;
		result[chan]=v;
	}
	return result;

}
float GetPedestal(std::vector<uint16_t> v)
{
  float avg = 0;
  for(unsigned int i=0;i<100;i++)
  {
    avg += v[i];
  }
  return avg/100;
}

float GetSignal(std::vector<uint16_t> v)
{
  float max = -10000;
  for(unsigned int i=0;i<v.size();i++)
  {
    if(v[i] > max) max = v[i];
  }
  return max;
}

std::vector<uint16_t> BuildChannel(PulseVec vec)
{
	std::vector<uint16_t> result(1024,0);
	for(auto pul : vec)
	{
		for(unsigned int i=0;i<pul.length;i++)
		{
			result[pul.start+i]=pul.samples[i];
		}
	}
	return result;
}


int main(int argc, char **argv){
    if (argc < 2){
        std::cout << "Usage: " << argv[0] << " <File Name> \n";
        return -1;
    }

    std::string fname = argv[1];

    std::cout << "File name = " << fname << std::endl;

    auto hists = std::map<std::string, TH1D*>();
	std::ifstream input(fname, std::ios::binary | std::ios::ate);
	auto pos = input.tellg();
        std::cout << "input.tellg() = " << pos << std::endl;
	std::vector<uint8_t>  data(pos);
	input.seekg(0, std::ios::beg);
    input.read(reinterpret_cast<char*>(&data[0]), pos);

    int offsetDtata = 0;
    int iEvent = 0;


    TFile f("hists.root", "RECREATE");
    TTree tree("Modules", "Modules");

    TString tree_structure("board_0_ch_0/f:board_0_ch_1/f");    
    float tree_buf[2] = {1,2};
    tree.Branch("Test", tree_buf, tree_structure);

    std::map<int, int> boardEventId;

    while (offsetDtata < pos) {

      auto n_bytes = *(reinterpret_cast<uint32_t*>(&data[offsetDtata]));
      auto evID = *(reinterpret_cast<uint64_t*>(&data[offsetDtata + 19*6]));
      auto iBoard = *(reinterpret_cast<uint16_t*>(&data[offsetDtata + 1*6 + 1])) & 0xfff;

      std::cout<<"Event with trigNumber =  "<< evID << ", iBoard = 0x" << std::hex << iBoard << std::dec << " contains "<<n_bytes<<" data bytes (excluding header)"<<std::endl;

      auto search = boardEventId.find(iBoard);
      if(search != boardEventId.end()) {
        auto iEvent = search->second + 1;
        boardEventId[iBoard] = iEvent;
      } else {
        boardEventId[iBoard] = 0;
      }
      std::cout << "Will process board " << iBoard << ", event " << boardEventId[iBoard] << std::endl;

      offsetDtata += HEADER_SIZE + n_bytes;


    gStyle->SetTickLength(0.001,"x");

    auto data_bytes = *(reinterpret_cast<uint32_t*>(&data[offsetDtata]));

    std::vector<uint16_t> data_16;

    for(unsigned int i = offsetDtata; i < offsetDtata + data_bytes; i+=3)
    {
    	uint16_t b0, b1, b2;
    	uint16_t samp0=0, samp1=0;
    	b0=data[HEADER_SIZE+i];
    	b1=data[HEADER_SIZE+i+1];
    	b2=data[HEADER_SIZE+i+2];


    	samp0=b0 |  ((b1) & 0x0F) <<8;
    	samp1=( (b1>>4) & 0xFF ) | b2<<4;
    	data_16.push_back(samp0&0xfff);
    	data_16.push_back(samp1&0xfff);


    }


    auto m = ParseData(data_16);
    //TCanvas *c = new TCanvas("c","",1024,768);
    std::vector<uint16_t> tslice(1024);
    std::iota(tslice.begin(),tslice.end(),1);

//    TCanvas *c=new TCanvas("c","",1024,768);
//    c->SetMargin(0.01,0.01,0.01,0.01);
//    c->Divide(4,1,0,0);
//    TMultiGraph** mg=new TMultiGraph*[4];
/*
    for (int i = 0; i<4; i++) {
      mg[i]=new TMultiGraph();
    }
*/

    for(auto& [ch,vec]:m )
    {
    	auto v = BuildChannel(vec);
        char hist_name[100];

        snprintf(hist_name, 100, "h_%d_%d", iBoard, ch+1);
        std::string h_name = std::string(hist_name);
        auto search = hists.find(h_name);
        if(search == hists.end()) {
          std::cout << "Creating hist " << h_name << " (" << hists.size() << ")" <<std::endl;
          hists[h_name] = new TH1D(hist_name, hist_name, 4096, 1, 4096);
        }
        auto value = GetSignal(v) - GetPedestal(v);

        std::cout << "Filling hist " << h_name << " with value " << value << std::endl;
        hists[hist_name]->Fill(value); 
 /*   	auto g= new TGraph(1024);

        std::cout << int(ch) << " " << v.size() << std::endl;
        int iCh = (int)ch;

    	for(int i=0;i<1024;i++)
    	{
          g->SetPoint(i,tslice[i],v[i] + 4096.*(iCh % 8));
    	}

        if (iCh < 32) mg[iCh / 8]->Add(g);
*/
    }
/*
	for(int i=0;i<4;i++)
        {
            c->cd(i+1);
            mg[i]->Draw("AL");
            mg[i]->SetMinimum(-100);
            mg[i]->SetMaximum(8*4096. + 100);
        }
*/
    }

    f.Write();
}
