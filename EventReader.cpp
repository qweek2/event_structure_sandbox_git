#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <numeric>
#include "utils.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TString.h"
#include "TH1D.h"

std::map<uint8_t,PulseVec> ParseData(std::vector<uint16_t> data)
{
	std::map<uint8_t,PulseVec> result;
	unsigned int word=0;
	while(word<data.size())
	{
		//std::cout<<"word: "<<word<<std::endl;
		int chan=data[word+2]+9*data[word+3];
		uint32_t words_to_read = data[word];
		words_to_read |= uint32_t(data[word+1])<<12;

		//std::cout<<"words to read: "<<words_to_read<<std::endl;

		PulseVec v;
		PulseData pul;
		int samples_to_read=0;
		std::vector<uint16_t> samples;
		word+=4;
		for(unsigned int i=0;i<words_to_read*4;)
		{
			if(samples_to_read==0)
			{
				pul.length=data[word+i];
				pul.start=data[word+i+1];
				i+=2;
				samples_to_read=pul.length;
				//std::cout<<"samples to read: "<<samples_to_read<<std::endl;
				continue;
			}
			samples.push_back(data[i+word]);
			i++;
			samples_to_read--;
			if(samples_to_read==0)
			{
				pul.samples=samples;
				v.push_back(pul);
				samples.clear();
			}

		}
		word+=words_to_read*4;
		result[chan]=v;
	}
	return result;

}

std::vector<uint16_t> BuildChannel(PulseVec vec)
{
	std::vector<uint16_t> result(1024,0);
	for(auto pul : vec)
	{
		for(unsigned int i=0;i<pul.length;i++)
		{
			result[pul.start+i]=pul.samples[i];
		}
	}
	return result;
}


int main(){

	std::ifstream input("dummy_event.bin", std::ios::binary | std::ios::ate);
	auto pos = input.tellg();
        std::cout << "input.tellg() = " << pos << std::endl;
	std::vector<uint8_t>  data(pos);
	input.seekg(0, std::ios::beg);
    input.read(reinterpret_cast<char*>(&data[0]), pos);
    auto data_bytes = *(reinterpret_cast<uint32_t*>(&data[0]));
    auto evID = *(reinterpret_cast<uint32_t*>(&data[21*6+4]));
    std::cout<<"Event with ID: "<<evID<<" contains "<<data_bytes<<" data bytes (excluding header)"<<std::endl;

    //get channel data
    // not used std::vector<uint8_t> channelData(data.begin()+HEADER_SIZE,data.end());
    std::vector<uint16_t> data_16;
    for(unsigned int i = 0; i < data_bytes; i+=3)
    {
    	uint16_t b0, b1, b2;
    	uint16_t samp0=0, samp1=0;
    	b0=data[HEADER_SIZE+i];
    	b1=data[HEADER_SIZE+i+1];
    	b2=data[HEADER_SIZE+i+2];


    	samp0=b0 |  ((b1) & 0x0F) <<8;
    	samp1=( (b1>>4) & 0xFF ) | b2<<4;
    	data_16.push_back(samp0&0xfff);
    	data_16.push_back(samp1&0xfff);


    }


    auto m = ParseData(data_16);
    TCanvas *c = new TCanvas("c","",1024,768);
    std::vector<uint16_t> tslice(1024);
    std::iota(tslice.begin(),tslice.end(),1);

    c->Print("wvfms.pdf[");
    auto frame = c->DrawFrame(0,-100,1024,4192);
    for(auto& [ch,vec]:m )
    {
    	auto v = BuildChannel(vec);
    	auto g=new TGraph(1024);
    	for(int i=0;i<1024;i++)
    	{
    		g->SetPoint(i,tslice[i],v[i]);
    	}
    	g->SetTitle(Form("channel %d",ch));
    	//frame->Draw();
    	g->Draw("ALP");
    	g->GetYaxis()->SetRangeUser(-100,4192);
    	g->GetXaxis()->SetRangeUser(0,1024);
    	c->Print("wvfms.pdf");
    }
    c->Print("wvfms.pdf]");

        std::cout << "input.tellg() = " << pos << std::endl;

}
