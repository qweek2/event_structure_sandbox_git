#include <iostream>
#include <fstream>
#include <vector>
#include <TFile.h>
#include <TNtuple.h>
#include <TGraph.h>
#include <TF1.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <numeric>
#include <stdio.h>
#include "utils.h"
#include "TString.h"
#include "TH1D.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "map.h"

using namespace std;

struct m_n_s
{
    int module;
    int section;
};

struct m_n_s find_module_and_section(int board, int ch)
{
    //see map.h for the BoardId; SocketId, ModuleId, SectionId, ChannelId defenitions and the map itself
    if (ChannelMapping.find(ch) == ChannelMapping.end())
    {
        //std::cout << "Channel mapping: ch #" << int(ch) << "NC, skipping" << std::endl;
    }
    auto [socket_id, section_id] = ChannelMapping[ch];
    if (ModuleMapping.find(board) == ModuleMapping.end())
    {
        //std::cout << "Module mapping: board #" << int(board) << " not recognized" << std::endl;
    }
    auto module_id = ModuleMapping[board][socket_id];

    //std::cout << "Board " << board << " channel " << int(ch) << " maps to PSD module " << module_id << " section " << section_id << std::endl;

    m_n_s result = {module_id, section_id};
    return result;
}

void v_to_s(std::vector<string> const &input)
{
    std::string result;
    for (auto const &s : input)
        result += s;
    cout << result << endl;
}

//print file names vector
void print(std::vector<string> const &input)
{
    for (int i = 0; i < input.size(); i++)
        std::cout << input.at(i) << ' ' << endl;
    ;
}

void PSD_cut()
{
    TCanvas *c1 = new TCanvas();
    c1->DivideSquare(10);

    vector<string> leaf_names_to_print;
    TH1F *Edep = new TH1F("Edep", "Edep", 1000, 0, 10000); //+
    TH1F *Section[10];
    for (int i = 0; i < 10; i++)
    {
        Section[i] = new TH1F(TString::Format("amp%d", i), TString::Format("amp%d", i), 100, 0., 1000.);
    }

    TFile *_file0 = TFile::Open("/mnt/c/Work/root/builddir/macros/hists.root");
    TTree *Modules = (TTree *)_file0->Get("Modules");
    TBranch *Test = (TBranch *)Modules->GetBranch("Test");

    //Find boards n sections
    int module_to_find = -1;
    cout << "Enter module number" << endl;
    cin >> module_to_find;
    for (int board_n = 1280; board_n < 1291; board_n++) //find boards n sections
        for (int n_ch = 0; n_ch < 36; n_ch++)
        {
            struct m_n_s result;
            result = find_module_and_section(board_n, n_ch);
            if (result.module == module_to_find)
            {
                cout << "Board " << board_n << " Channel " << n_ch << " --> Module " << result.module << " Section " << result.section << endl;
                leaf_names_to_print.push_back("Amp_board_" + std::to_string(board_n) + "_ch_" + std::to_string(n_ch));
            }
        }

    cout << "List of leaves" << endl;
    print(leaf_names_to_print); //check if the list is correct

    //loop over entries
    for (int j = 0; j < Modules->GetEntries(); j++)
    {
        Modules->GetEntry(j);
        int i_array = 0;
        Float_t sum_energy = 0;
        for (auto i_name = leaf_names_to_print.begin(); i_name != leaf_names_to_print.end(); ++i_name)
        {
            Float_t value = 0;
            TString name = *i_name;
            if (Modules->GetListOfLeaves()->FindObject(name) != 0) //check if the leaf exist
                value = Test->GetLeaf(name)->GetValue(0);
            else
                cout << "Leaf " << name << " not found. Entry =  " << j << " " << Modules->GetListOfLeaves()->FindObject(name) << endl;
            if (value > 0.001 && value < 100000)
                sum_energy += value;
        }
        Edep->Fill(sum_energy);
        if (true) //sum_energy < 900) //CUTS
            for (auto i_name = leaf_names_to_print.begin(); i_name != leaf_names_to_print.end(); ++i_name)
            {
                Float_t value = 0;
                TString name = *i_name;
                if (Modules->GetListOfLeaves()->FindObject(name) != 0) //check if the leaf exist
                {
                    value = Test->GetLeaf(name)->GetValue(0);
                    if (value > 0.001 && value < 100000) //check if the value is adequate
                        Section[i_array]->Fill(value);
                }
                else
                    cout << "Leaf " << name << " not found. Entry =  " << j << endl;
                i_array++;
            }
    }
    int i_array = 0;
    for (auto i_name = leaf_names_to_print.begin(); i_name != leaf_names_to_print.end(); ++i_name)
    {
        TString name = *i_name;
        c1->cd(i_array + 1);
        Section[i_array]->Draw();
        Section[i_array]->SetTitle(name);
        i_array++;
    }

    TCanvas *c2 = new TCanvas();
    Edep->SetTitle("Sum");
    Edep->Draw();
}

void PSD_5_vs_5()
{
    TCanvas *c1 = new TCanvas();
    c1->DivideSquare(10);
    vector<string> leaf_names_to_print;
    TH2F *Edep = new TH2F("Edep", "Edep", 1000, 0, 5000, 1000, 0, 5000);

    TFile *_file0 = TFile::Open("/mnt/c/Work/root/builddir/macros/hists.root");
    TTree *Modules = (TTree *)_file0->Get("Modules");
    TBranch *Test = (TBranch *)Modules->GetBranch("Test");

    //Find boards n sections
    int module_to_find = -1;
    cout << "Enter module number" << endl;
    cin >> module_to_find;
    for (int board_n = 1280; board_n < 1291; board_n++) //find boards n sections
        for (int n_ch = 0; n_ch < 36; n_ch++)
        {
            struct m_n_s result;
            result = find_module_and_section(board_n, n_ch);
            if (result.module == module_to_find)
            {
                cout << "Board " << board_n << " Channel " << n_ch << " --> Module " << result.module << " Section " << result.section << endl;
                leaf_names_to_print.push_back("Amp_board_" + std::to_string(board_n) + "_ch_" + std::to_string(n_ch));
            }
        }

    cout << "List of leaves" << endl;
    print(leaf_names_to_print); //check if the list is correct

    //declaration should be here
    TH1F *Section[10];
    for (int i = 0; i < 10; i++)
    {
        Section[i] = new TH1F(TString::Format("s%d", i), TString::Format("s%d", i), 100, 0, 1000);
    }

    //set cut
    int cut_point = 1600;

    //loop over entries
    for (int j = 0; j < Modules->GetEntries(); j++)
    {
        Modules->GetEntry(j);
        int i_array = 0;
        Float_t sum_energy_1_5 = 0, sum_energy_6_10 = 0;
        for (auto i_name = leaf_names_to_print.begin(); i_name != leaf_names_to_print.end(); ++i_name)
        {
            Float_t value = 0;
            TString name = *i_name;
            if (Modules->GetListOfLeaves()->FindObject(name) != 0) //check if the leaf exist
                value = Test->GetLeaf(name)->GetValue(0);
            else
                cout << "Leaf " << name << " not found. Entry =  " << j << " " << Modules->GetListOfLeaves()->FindObject(name) << endl;

            if (i_array < 5 && value > 0.001 && value < 10000)
                sum_energy_1_5 += value;
            else if (value > 0.001 && value < 10000)
                sum_energy_6_10 += value;
            i_array++;
        }
        Edep->Fill(sum_energy_1_5, sum_energy_6_10);

        i_array = 0;
        if (sum_energy_1_5 > cut_point && sum_energy_6_10 > cut_point) //CUTS
            for (auto i_name = leaf_names_to_print.begin(); i_name != leaf_names_to_print.end(); ++i_name)
            {
                Float_t value = 0;
                TString name = *i_name;
                if (Modules->GetListOfLeaves()->FindObject(name) != 0) //check if the leaf exist
                {
                    value = Test->GetLeaf(name)->GetValue(0);
                    if (value > 0.001 && value < 100000)
                        Section[i_array]->Fill(value);
                }
                else //this two lines may be commented
                    cout << "Leaf " << name << " not found. Entry =  " << j << endl;
                i_array++;
            }
    }

    int i_array = 0;
    for (auto i_name = leaf_names_to_print.begin(); i_name != leaf_names_to_print.end(); ++i_name)
    {
        TString name = *i_name;
        c1->cd(i_array + 1);
        Section[i_array]->Draw();
        Section[i_array]->SetTitle(name);
        i_array++;
    }

    TCanvas *c2 = new TCanvas();
    Edep->SetTitle("5 vs 5");
    Edep->GetYaxis()->SetTitle("6 to 10");
    Edep->GetXaxis()->SetTitle("1 to 5");
    Edep->Draw();

    //show cut lines

    TLine *line_y = new TLine(cut_point, cut_point, cut_point, 5000);
    line_y->SetLineWidth(2);
    line_y->SetLineColor(3);
    line_y->Draw();

    TLine *line_x = new TLine(cut_point, cut_point, 5000, cut_point);
    line_x->SetLineWidth(2);
    line_x->SetLineColor(3);
    line_x->Draw();
}