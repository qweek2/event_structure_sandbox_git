#include <vector>
#include <map>
#include <fstream>
#include <random>
#include <math.h>
#include <iostream>
#include "utils.h"

std::random_device rd;
std::mt19937 mt(rd());
std::uniform_real_distribution<double> dist(0., 1.);


std::vector<char> GenerateHeader(uint32_t data_bytes, uint32_t event_ID)
{
	std::vector<char> v(HEADER_SIZE,0); //prepare space
	auto len = reinterpret_cast<uint32_t*>(&v[0]);
	*len=data_bytes;
	auto evid=reinterpret_cast<uint32_t*>(&v[21*6+4]);
	*evid=event_ID;
	return v;
}




PulseData GeneratePulse(uint16_t pos, uint16_t amp)
{
	std::vector<uint16_t> v; //fixed size, 101 samples
	for(auto i=0;i<101;i++)
	{
		auto s= amp*exp(-0.005*pow(i-50.,2))+100;
		v.push_back(s);
	}
	return {101, pos, v};

}

std::map<uint8_t, PulseVec> GenerateChannels()
{
	std::map<uint8_t, PulseVec> result;
	for(int i=0;i<36;i++)
	{
		//50% occupancy
		if(dist(mt)<0.5)
			continue;
		result[i]={GeneratePulse(dist(mt)*800+10,dist(mt)*3000)};
	}
	return result;
}

//dummy approach
std::vector<char> GenerateData(std::map<uint8_t, PulseVec> data)
{
	std::vector<uint64_t> v; //to store 48 bit words - 16 bits ber word are lost
	for (auto& [chan, chan_data]: data)
	{
		int n12_bit_words=0; //number of 12-bit words
		for(auto d : chan_data)
			n12_bit_words+=d.length+2;
		uint32_t n48_bit_words= (n12_bit_words-1)/4+1;
		uint64_t val=0;
		uint64_t drsID = chan/9;
		uint64_t chanID = chan%9;
		//val= (n48_bit_words & (0x00000000FFFFFFFF)); 
		//val|=(chanID<<32)  &  (0x000000FF00000000);
		//val|=(drsID<<40)  &   (0x0000FF0000000000);
		val= (n48_bit_words & (0x0000000000FFFFFF)); 
		val|=(chanID<<24)  &  (0x0000000FFF000000);
		val|=(drsID<<36)  &   (0x0000FFF000000000);
		v.push_back(val); //channel header
		//loop over 'pulses'
		val=0;
		for(auto d : chan_data)
		{
			int nWord=0; //count 12-bit words
			uint64_t len, time;
			len=d.length;
			time=d.start;
			val|=len<<(nWord++*12);
			if(nWord==4)
			{
				v.push_back(val);
				val=0;
				nWord=0;
			}
			val|=time<<(nWord++*12);
			if(nWord==4)
			{
				v.push_back(val);
				val=0;
				nWord=0;
			}

			for(uint64_t s : d.samples)
			{
				val|=s<<(nWord++*12);
				if(nWord==4)
				{	
					v.push_back(val);
					val=0;
					nWord=0;
				}
			}
		}
		v.push_back(val);
	}
	int n=0;
	//hacky way to convert to bytes
	//48 bits of each 64-bit int are stored
	std::vector<char> vout(v.size()*6+2,0);
	for(auto a : v)
	{
		auto ptr = reinterpret_cast<uint64_t*>(&vout[n]);
		*ptr=a;
		n+=6;
	}
	//remove 2 last bytes 
	vout.erase(vout.end()-2,vout.end());
	return vout;
}

int main()
{
	auto d = GenerateChannels();

	auto data = GenerateData(d);
	
	auto header =GenerateHeader(data.size(),0xDEADFACE);

	std::ofstream fout("dummy_event.bin", std::ios::out | std::ofstream::binary);
	//write header to file
	std::copy(header.begin(), header.end(), std::ostreambuf_iterator<char>(fout));
	//write data to file
	std::copy(data.begin(), data.end(), std::ostreambuf_iterator<char>(fout));

	fout.close();

}