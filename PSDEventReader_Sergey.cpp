#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <numeric>
#include <stdio.h>
#include "utils.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TString.h"
#include "TH1D.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"

typedef uint16_t BoardId ;
typedef uint16_t SocketId ;
typedef uint16_t ModuleId ;
typedef uint16_t SectionId ;
typedef uint16_t ChannelId ;

// How are the modules connected to the boards (via the sockets)
std::map<BoardId, std::map<SocketId, ModuleId> > ModuleMapping = {
  { 1280, {{1,21}, {2,34}, {3,33}} },
  { 1281, {{1,36}, {2,35}, {3,22}} },
  { 1282, {{1,20}, {2,32}, {3,31}} },
  { 1283, {{1,38}, {2,37}, {3,23}} },
  { 1284, {{1,30}, {2,18}, {3,19}} },
  { 1285, {{1,16}, {2,14}, {3,13}} },
  { 1286, {{1,24}, {2,25}, {3,39}} },
  { 1287, {{1,44}, {2,17}, {3,29}} },
  { 1288, {{1,28}, {2,27}, {3,15}} },
  { 1289, {{1,40}, {2,26}, {3,41}} },
  { 1290, {{1,43}, {2,42}} }
};

// How are the channels mapped in the sockets
std::map<ChannelId, std::pair<SocketId, SectionId> > ChannelMapping = {
  {  0, {1, 1} },
  {  1, {1, 2} },
  {  2, {1, 3} },
  {  3, {1, 4} },
  {  4, {1, 5} },
  {  5, {1, 6} },
  {  6, {1, 7} },
  {  7, {1, 8} },
//{  8, {0, 0} },  // Time Ref 1
  {  9, {1, 9} },
  { 10, {1,10} },
//{ 11, {0, 0} },  // NC
  { 12, {2, 1} },
  { 13, {2, 2} },
  { 14, {2, 3} },
  { 15, {2, 4} },
  { 16, {2, 5} },
//{ 17, {0, 0} },  // Time Ref 2
  { 18, {2, 6} },
  { 19, {2, 7} },
  { 20, {2, 8} },
  { 21, {2, 9} },
  { 22, {2,10} },
//{ 23, {0, 0} },  // NC
  { 24, {3, 1} },
  { 25, {3, 2} },
//{ 26, {0, 0} },  // Time Ref 3
  { 27, {3, 3} },
  { 28, {3, 4} },
  { 29, {3, 5} },
  { 30, {3, 6} },
  { 31, {3, 7} },
  { 32, {3, 8} },
  { 33, {3, 9} },
  { 34, {3,10} },
//{ 35, {0, 0} },
};

/* Example:
 * with board_id and channel_id:
 *
   if(ChannelMapping.find(channel_id) == ChannelMapping.end()) break; // Channel NC
   auto [socket_id, section_id] = ChannelMapping[channel_id];
   if(ModuleMapping.find(board_id) == ModuleMapping.end()) break; // Unknown board
   auto module_id = ModuleMapping[board_id][socket_id];
*/

std::map<uint8_t,PulseVec> ParseData(std::vector<uint16_t> data)
{
	std::map<uint8_t,PulseVec> result;
	unsigned int word=0;
	while(word<data.size())
	{
		//std::cout<<"word: "<<word<<std::endl;
		int chan=data[word+2]+9*data[word+3];
		uint32_t words_to_read = data[word];
		words_to_read |= uint32_t(data[word+1])<<12;

		//std::cout<<"words to read: "<<words_to_read<<std::endl;

		PulseVec v;
		PulseData pul;
		int samples_to_read=0;
		std::vector<uint16_t> samples;
		word+=4;
		for(unsigned int i=0;i<words_to_read*4;)
		{
			if(samples_to_read==0)
			{
				pul.length=data[word+i];
				pul.start=data[word+i+1];
				i+=2;
				samples_to_read=pul.length;
				//std::cout<<"samples to read: "<<samples_to_read<<std::endl;
				continue;
			}
			samples.push_back(data[i+word]);
			i++;
			samples_to_read--;
			if(samples_to_read==0)
			{
				pul.samples=samples;
				v.push_back(pul);
				samples.clear();
			}

		}
		word+=words_to_read*4;
		result[chan]=v;
	}
	return result;

}
float GetPedestal(std::vector<uint16_t> v, int iBeg = -1, int iEnd = -1)
{
  float avg = 0;

  if (iBeg < 0) iBeg = 0;
  if (iEnd < 0) iEnd = v.size();

  for(unsigned int i = iBeg; i < iEnd; i++) {
    avg += v[i];
  }
  return avg/(iEnd - iBeg + 1);
}

float GetSignal(std::vector<uint16_t> v, int iBeg = -1, int iEnd = -1)
{
  float max = -10000;

  if (iBeg < 0) iBeg = 0;
  if (iEnd < 0) iEnd = v.size();

  for(unsigned int i = iBeg; i < iEnd; i++) {
    if(v[i] > max) max = v[i];
  }
  return max;
}

float GetSignalTime(std::vector<uint16_t> v, int iBeg = -1, int iEnd = -1)
{
  float max = -10000;

  if (iBeg < 0) iBeg = 0;
  if (iEnd < 0) iEnd = v.size();

  float maxTime = iBeg;

  for(unsigned int i = iBeg; i < iEnd; i++) {
    if(v[i] > max) {
      max = v[i];
      maxTime = (float)i;
    }
  }
  return maxTime;
}

std::vector<uint16_t> BuildChannel(PulseVec vec)
{
	std::vector<uint16_t> result(1024,0);
	for(auto pul : vec)
	{
		for(unsigned int i=0;i<pul.length;i++)
		{
			result[pul.start+i]=pul.samples[i];
		}
	}
	return result;
}

// First pass over input file (first event only) to get the list of board ids recorded
//
std::vector<uint16_t> ReadBoardIds(std::ifstream& input) {
    std::cout << "=== First pass: get board IDs ===" << std::endl;
    std::vector<uint16_t> board_ids;

    std::vector<uint8_t>  header(HEADER_SIZE);

    input.seekg(0, std::ios::beg);
    input.read(reinterpret_cast<char*>(&header[0]), HEADER_SIZE);

    int n_bytes = *(reinterpret_cast<uint32_t*>(&header[0]));

    //std::map<int, int> boardEventId;
    int firstEvID = *(reinterpret_cast<uint32_t*>(&header[21*6 + 4]));
    int evID = firstEvID;
    int iBoard = *(reinterpret_cast<uint16_t*>(&header[1*6 + 1])) & 0xfff;


    while (evID <= firstEvID) {

      std::cout<<"Event with evID =  "<< evID << ", iBoard = 0x" << std::hex << iBoard << std::dec << " contains "<<n_bytes<<" data bytes (excluding header)"<<std::endl;

      std::vector<uint8_t>  data(n_bytes);
      input.read(reinterpret_cast<char*>(&data[0]), n_bytes);

      //read next event header
      input.read(reinterpret_cast<char*>(&header[0]), HEADER_SIZE);
      evID = *(reinterpret_cast<uint32_t*>(&header[21*6 + 4]));
      iBoard = *(reinterpret_cast<uint16_t*>(&header[1*6 + 1])) & 0xfff;

      board_ids.push_back(iBoard);

    }


    input.seekg(0, std::ios::beg);
    std::cout << "=== First pass: done (" << board_ids.size() << " boards)" << std::endl;

    std::sort(board_ids.begin(), board_ids.end());
    return board_ids;
}

TString TreeStructureForBoards(std::vector<uint16_t> board_ids) {
    TString result;
    int nChannels = 36;
    for(int i = 0; i < board_ids.size(); i++) {
        for(int sec = 0; sec < nChannels; sec++) {
            result += TString::Format("Amp_board_%d_ch_%d/f:", board_ids[i], sec);
            if (i == (board_ids.size() - 1) && sec == (nChannels - 1)) {
              result += TString::Format("Time_board_%d_ch_%d/f", board_ids[i], sec);
            }
            else result += TString::Format("Time_board_%d_ch_%d/f:", board_ids[i], sec);
        }
    }
    return result;
}


int FindBoardOrderById(std::vector<uint16_t> board_ids, uint16_t id) {
    for(int i = 0; i < board_ids.size(); i++) {
        if(board_ids[i] == id) { return i; }
    }
    return -1;
}

int main(int argc, char **argv){
    if (argc < 3){
        std::cout << "Usage: " << argv[0] << " <File Name(s)> <nEventsToRead>\n";
        return -1;
    }

    int nFiles = argc - 2;
    std::vector<std::string>  fileNames;
    for (int iName = 0; iName < nFiles; iName++) fileNames.push_back(argv[iName + 1]);

    std::cout << "File names from " << fileNames[0] << " till " << fileNames[fileNames.size() - 1] << std::endl;

    int nEventsToRead = atoi(argv[argc -1]);

    std::cout << "nEventsToRead = " << nEventsToRead << std::endl;

    auto hists = std::map<std::string, TH1D*>();

    int currentFile = 0;

    std::ifstream input(fileNames[currentFile], std::ios::binary | std::ios::ate);
    auto pos = input.tellg();
    std::cout << "input.tellg() = " << pos << std::endl;

    auto board_ids = ReadBoardIds(input);

    TFile f("hists.root", "RECREATE");
    TTree tree("Modules", "Modules");

    auto tree_structure = TreeStructureForBoards(board_ids);
    //std::cout << tree_structure << std::endl;
    //float *tree_buf = new float[board_ids.size()*36*2];

    std::cout << "board_ids.size() = " << board_ids.size() << std::endl;

    float tree_buf[board_ids.size()*36*2];
    tree.Branch("Test", tree_buf, tree_structure);

/*
    input.read(reinterpret_cast<char*>(&header[0]), HEADER_SIZE);

    int n_bytes = *(reinterpret_cast<uint32_t*>(&header[0]));

    int firstEvID = *(reinterpret_cast<uint32_t*>(&header[21*6 + 4]));
    int evID = firstEvID;
    int iBoard = *(reinterpret_cast<uint16_t*>(&header[1*6 + 1])) & 0xfff;


    std::vector<uint8_t>  data(pos);

    input.read(reinterpret_cast<char*>(&data[0]), pos);
*/

    int iEvent = 0;
    int lastEvID = -1;

    //std::map<int, int> boardEventId;

    int eventsDone = 0;

    std::cout << std::endl << "===Second pass: Read Events ===" << std::endl;
    input.seekg(0, std::ios::beg);

    int relativeEventID = 0;

    while (true) {

      if (nEventsToRead > 0 && eventsDone >= nEventsToRead) break;

      std::vector<uint8_t>  header(HEADER_SIZE);
      input.read(reinterpret_cast<char*>(&header[0]), HEADER_SIZE);
      if (input.eof()) {
        //switch to next file
        std::cout << "Closing file " << fileNames[currentFile] << std::endl;
        input.close();
        input.clear();
        currentFile++;
        if (currentFile > (fileNames.size() - 1)) break; //no more files
        std::cout << "Opening file " << fileNames[currentFile] << std::endl;
        input.open(fileNames[currentFile], std::ios::binary | std::ios::ate);
        input.seekg(0, std::ios::beg);
        input.read(reinterpret_cast<char*>(&header[0]), HEADER_SIZE);
      }

      auto n_bytes = *(reinterpret_cast<uint32_t*>(&header[0]));
      auto evID = *(reinterpret_cast<uint32_t*>(&header[21*6 + 4]));
      auto iBoard = *(reinterpret_cast<uint16_t*>(&header[1*6 + 1])) & 0xfff;

      std::vector<uint8_t>  data(n_bytes);
      input.read(reinterpret_cast<char*>(&data[0]), n_bytes);

      int boardOrder = FindBoardOrderById(board_ids, iBoard);
      int nChannels = 36;

      if(!(evID%100)) std::cout<<"Event with trigNumber =  "<< evID << ", iBoard = 0x" << std::hex << iBoard << std::dec << " contains "<<n_bytes<<" data bytes (excluding header)"<<std::endl;

/*
      auto search = boardEventId.find(iBoard);
      if(search != boardEventId.end()) {
        auto iEvent = search->second + 1;
        boardEventId[iBoard] = iEvent;
      } else {
        boardEventId[iBoard] = 0;
      }
*/
      if(lastEvID < 0) {
          lastEvID = evID;
      }

      if(lastEvID != evID) {
          // new event; need to write down the last one
          tree.Fill();
          lastEvID = evID;
          relativeEventID++;
          //std::cout << "Tree.Fill()" << lastEvID << " " << evID << std::endl;
      }

      //std::cout << "Will process board " << iBoard << ", event " << boardEventId[iBoard] << std::endl;

    //gStyle->SetTickLength(0.001,"x");

    std::vector<uint16_t> data_16;

    for(unsigned int i = 0; i < n_bytes; i+=3)
    {
    	uint16_t b0, b1, b2;
    	uint16_t samp0=0, samp1=0;
    	b0=data[i];
    	b1=data[i+1];
    	b2=data[i+2];


    	samp0=b0 |  ((b1) & 0x0F) <<8;
    	samp1=( (b1>>4) & 0xFF ) | b2<<4;
    	data_16.push_back(samp0&0xfff);
    	data_16.push_back(samp1&0xfff);


    }

    auto m = ParseData(data_16);
    std::vector<uint16_t> tslice(1024);
    std::iota(tslice.begin(),tslice.end(),1);

    //for(auto& [ch,vec]:m )
    for(auto it=m.begin(); it != m.end(); ++it)
    {
        auto ch = it->first;
        auto vec = it->second;

        // Find PSD module and section connected to given board and channel
        if(ChannelMapping.find(ch) == ChannelMapping.end()) {
            //std::cout << "Channel mapping: ch #" << int(ch) << "NC, skipping" << std::endl;
            continue; // Channel NC
        }
        auto [socket_id, section_id] = ChannelMapping[ch];

        if(ModuleMapping.find(iBoard) == ModuleMapping.end()) {
            std::cout << "Module mapping: board #" << int(iBoard) << " not recognized" << std::endl;
            continue; // Unknown board
        }
        auto module_id = ModuleMapping[iBoard][socket_id];

        //std::cout << "Board " << iBoard << " channel " << int(ch) << " maps to PSD module " << module_id << " section " << section_id << std::endl;

        //hehe
        int pedBeg = 50; int pedEnd = 100;
        int sigBeg = 100; int sigEnd = 500;

    	auto v = BuildChannel(vec);
        auto value = GetSignal(v, sigBeg, sigEnd) - GetPedestal(v, pedBeg, pedEnd);
        auto valueTime = GetSignalTime(v, sigBeg, sigEnd);


        char hist_name[100];

        // Find or create per-section histogram
        //snprintf(hist_name, 101, "h_%d_%d", iBoard, ch+1);
        sprintf(hist_name, "hEdepSect%d", module_id*100 + section_id);
        auto search = hists.find(std::string(hist_name));
        if(search == hists.end()) {
          //std::cout << "Creating hist " << hist_name << " (" << hists.size() << ")" <<std::endl;
          hists[hist_name] = new TH1D(hist_name, hist_name, 400, -100, 4096);
        }

        //std::cout << "Filling hist " << h_name << " with value " << value << std::endl;
        hists[hist_name]->Fill(value);

        //fill waveforms
        if (relativeEventID < 40) {
          sprintf(hist_name, "hADCEvent%d", relativeEventID*10000 + module_id*100 + section_id);
          auto search = hists.find(std::string(hist_name));
          if(search == hists.end()) {
            //std::cout << "Creating hist " << hist_name << " (" << hists.size() << ")" <<std::endl;
            hists[hist_name] = new TH1D(hist_name, hist_name, 1024, 0, 1023);
          }
          for(unsigned int i = 0; i < v.size(); i++) hists[hist_name]->SetBinContent(i,v[i]);
        }

        // Find or create per-module histogram
        // TODO sum up the modules
        sprintf(hist_name, "hEdepMod%d", module_id);
        search = hists.find(std::string(hist_name));
        if(search == hists.end()) {
          //std::cout << "Creating hist " << hist_name << " (" << hists.size() << ")" <<std::endl;
          hists[hist_name] = new TH1D(hist_name, hist_name, 400, -100, 4096);
        }

        //std::cout << "Filling hist " << hist_name << " with value " << value << std::endl;
        hists[hist_name]->Fill(value);


        // Fill the tree
        //tree_buf[boardOrder*36+ch] = value;
        tree_buf[(boardOrder*36+ch)*2] = value;
        tree_buf[(boardOrder*36+ch)*2+1] = valueTime;

    } //for(auto& [ch,vec]:m )

      eventsDone++;

  } // while

    tree.Write();
    f.Write();
    f.Close();


    return 0;
}
